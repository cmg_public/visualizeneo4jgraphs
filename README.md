This source code is intended to support my blog on Visualizing Neo4j Graphs in Jupyter Notebooks published at https://www.515tech.com/post/how-to-visualize-neo4j-graphs-in-jupyter-notebooks  
Please contact me if you have any questions or comments. adidonato@515tech.com
